<?php

namespace Drupal\common_custom\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigForm.
 */
class ConfigForm extends ConfigFormBase {

    /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
    protected $requestStack;

    /**
   * {@inheritdoc}
   */
    public static function create(ContainerInterface $container) {
        $instance = parent::create($container);
        $instance->requestStack = $container->get('request_stack');
        return $instance;
    }


    /**
   * {@inheritdoc}
   */
    protected function getEditableConfigNames() {
        return [
            'common_custom.config',
        ];
    }

    /**
   * {@inheritdoc}
   */
    public function getFormId() {
        return 'config_form';
    }

    /**
   * {@inheritdoc}
   */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config('common_custom.config');
        $form['bodyClass'] = [
            '#type' => 'fieldset',
            '#title' => $this->t('Body Class'),
        ];
        
        $form['bodyClass']['append_url_class']= [
            '#type' => 'checkbox',
            '#title' => $this->t('Append Url To Body Class'),
            '#default_value' => $config->get('append_url_class'),
        ];
        $form['bodyClass']['append_user_roles_class']= [
            '#type' => 'checkbox',
            '#title' => $this->t('Append User roles To Body Class'),
            '#default_value' => $config->get('append_user_roles_class'),
        ];

        $form['jsHelpers'] = [
            '#type' => 'fieldset',
            '#title' => $this->t('JS/DOM helper')
        ];

        $form['jsHelpers']['doJs'] = [
            '#type' => 'checkbox',
            '#title' => $this->t("Enable doJs for this site"),
            '#description' => 'See <a href="#">Page</a> for details<br/ > After enbaling <a  dojs dojs-click-toggle-text=\'["@self"],["Working, click again "]\' dojs1-click-toggle-text=\'["#testcaseofdojs"],["to reset...."]\'> CLICK here </a> <span id="testcaseofdojs">to test.</span>',
            '#default_value' => $config->get('doJs'),
        ];

        // checkbox, append url to body class
        return parent::buildForm($form, $form_state);
    }

    /**
   * {@inheritdoc}
   */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        parent::submitForm($form, $form_state);
        

        $this->config('common_custom.config')
            ->set('append_url_class', $form_state->getValue('append_url_class'))
            ->set('append_user_roles_class', $form_state->getValue('append_user_roles_class'))
            ->set('doJs', $form_state->getValue('doJs'))
            ->save();
    }

}
