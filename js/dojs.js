
(function(window, $){
    let dojs = {
        events:[],
        actions:{},
        addAction:function(name, fn){
            if(typeof fn == "function"){
                this.actions[name] = fn;
            }
        },
        getActionsList:function(){
            return Object.keys(this.actions);
        },
        getTarget:function($element, selector){
            let p;
            let c;
            
            switch(true)
            {
                case selector.length > 1:
                p = selector[0];
                c = selector[1];
                return p=="@self"?$element.find(c):$element.closest(p).find(c);
                break;
                case selector.length === 1:
                c = selector[0];
                return c=="@self"?$element:$(c);
                break;
                default:
                return null;
                break;
            }
        },
        applyOnDom:function(){
            $("[dojs]").each(function(){
                
                let $this = $(this);
                let _dojsAttr = this.getAttributeNames().filter(e=>e.indexOf("dojs") === 0 && e!=="dojs");
                for(let attrName of _dojsAttr)
                {
                    let value = $this.attr(attrName);
                    let _valueObj = JSON.parse(`[${value}]`);
                    let selector = _valueObj[0];
                    if(_valueObj.length<2){
                        console.warn("Value must be of format \"[selector|emptyString],[...otherValues]\" in Element", $this[0]);
                        continue;
                    }
                    let args = _valueObj[1];
                    let attributeParts = attrName.split("-");
                    let event = attributeParts[1];
                    let action = attributeParts[2];
                    let extraAttribs = attributeParts.slice(3);
                    let self = extraAttribs[extraAttribs.length-1] == "@s";
                    let $target = dojs.getTarget($this, selector);
                    
                    if(self){
                        extraAttribs.splice(extraAttribs.length-1);
                        self = $this;
                    }
                    
                    let _obj = {event, action, extraAttribs, args, value, $self:self, $target};
                    console.log(_obj);
                    $this.on(event, function(domEvent){
                        if(dojs.actions[action]){
                            dojs.actions[action]({domEvent, ..._obj});
                        }else{
                            console.warn('No action is set for dojs '+action+' is set. called from element', $this[0]);
                        }
                    })
                    
                }
                
            });
            
        }
    };
    window.dojs = dojs;
    
    //add some actions
    
    dojs.addAction('set', function({domEvent, event, action, extraAttribs, args, value, $target, $self}){
        //set $target to self if self is object
        if($self){
            $target = $self;
        }
        if(extraAttribs.length > 1)
        {
            
        }else{
            let actionT = extraAttribs[0];
            switch(actionT)
            {
                case "class":
                $target.attr('class', args[0]);
                break;
                case 'text':
                console.log('text', args, value);
                $target.text(args[0]);
                break;
                default: console.warn("Unknown action", actionT, " on ", $target);
            }   
        }
    });
    
    dojs.addAction('add', function({domEvent, event, action, extraAttribs, args, value, $target, $self}){
        //set $target to self if self is object
        if($self){
            $target = $self;
        }
        if(extraAttribs.length > 1)
        {
            
        }else{
            let actionT = extraAttribs[0];
            switch(actionT)
            {		
                case "class":
                $target.addClass(args[0]);
                break;
                case 'text':
                $target.text($target.text()+args[0]);
                break;
                default: console.warn("Unknown action'", actionT, "' on ", $target);
            }   
        }
    });
    
    dojs.addAction('toggle', function({domEvent, event, action, extraAttribs, args, value, $target, $self}){
        //set $target to self if self is object

        if($self){
            $target = $self;
        }
        if(extraAttribs.length > 1)
        {
            
        }else{
            let actionT = extraAttribs[0];
            switch(actionT)
            {		
                case "class":
                $target.toggleClass(args[0]);
                break;
                case 'text':
                if(!$target[0].dojsOrigText){
                    $target[0].dojsOrigText = $target.text();
                    $target.text(args[0]);
                }else{
                    $target.text($target[0].dojsOrigText);
                    $target[0].dojsOrigText = null;
                }
                break;
                default: console.warn("Unknown action '", actionT, "' on ", $target);
            }   
        }
    });
    
    dojs.addAction('remove', function(props){
        console.log('remove', props);
    });
    
    //set callback on event
    $(function(){
        dojs.applyOnDom();
    });
    
    
    
    
    
})(window, jQuery);